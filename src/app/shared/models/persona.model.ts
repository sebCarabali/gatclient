import { TipoDocumento } from './tipo-documento.model';
import { Contacto } from './contacto.model';

export class Persona {
    constructor(
        public id? : number,
        public tipoDocumento? : TipoDocumento,
        public identificacion? : string,
        public contacto? : Contacto
    ) { }
}