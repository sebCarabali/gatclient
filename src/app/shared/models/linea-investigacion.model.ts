import { GrupoInvestigacion } from './grupo-investigacion.model';

export class LineaInvestigacion{
    constructor(
        public id? : number,
        public tipoLinea? : string,
        public grupoInvestigacion? : GrupoInvestigacion
    ){}
}