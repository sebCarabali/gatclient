import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { JefeDepartamentoRoutingModule } from './jefe-departamento-routing.module'

import { StudentComponent } from './student/student/student.component';
import { StudentListComponent } from './student/student-list/student-list.component';
import { StudentDetailComponent } from './student/student-detail/student-detail.component';
import { ProfessorComponent } from './professor/professor/professor.component';
import { ProfessorListComponent } from './professor/professor-list/professor-list.component';
import { ProfessorDetailComponent } from './professor/professor-detail/professor-detail.component';
import { ProfessorBulkLoadComponent } from './professor/professor-bulk-load/professor-bulk-load.component';
import { ProfessorCreateComponent } from './professor/professor-create/professor-create.component';
import { FormsModule } from '@angular/forms';
import { FileUploadModule } from 'ng2-file-upload';
import { DocenteService } from './professor/docente.service';
import { StudentService } from './student/student.service';
import { StudentCreateComponent } from './student/student-create/student-create.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { BasicAuthHttpInterceptorService } from '../login/basic-auth-http-interceptor.service';


@NgModule({
  declarations: [StudentComponent, StudentListComponent, StudentDetailComponent, ProfessorComponent, ProfessorListComponent, ProfessorDetailComponent, ProfessorBulkLoadComponent, ProfessorCreateComponent, StudentCreateComponent],
  imports: [
    CommonModule,
    JefeDepartamentoRoutingModule,
    FormsModule,
    FileUploadModule
  ],
  providers: [
    DocenteService,
    StudentService, {
      provide : HTTP_INTERCEPTORS, useClass : BasicAuthHttpInterceptorService, multi : true
    }
  ]
})
export class JefeDepartamentoModule { }
