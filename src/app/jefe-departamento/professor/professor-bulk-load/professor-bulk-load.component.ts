import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FileUploader } from 'ng2-file-upload';

@Component({
  selector: 'app-professor-bulk-load',
  templateUrl: './professor-bulk-load.component.html',
  styleUrls: ['./professor-bulk-load.component.styl']
})
export class ProfessorBulkLoadComponent implements OnInit {

  @ViewChild('fileInput', {static: false}) fileInput : ElementRef;
  uploader : FileUploader;
  url : string;

  constructor() { 
    this.url = "http://localhost:8080/GAT/api/docentes/bulkload";
  }

  ngOnInit() {
    const headers = [{
      name : 'Accept',
      value : 'application/json'
    }];
    this.uploader = new FileUploader({
      url : this.url,
      autoUpload : false,
      headers : headers
    });
    this.uploader.onCompleteAll = () => alert('File uploaded');
  }

}
