import { Component, OnInit, Input } from '@angular/core';
import { DocenteService } from '../docente.service';
import { Docente } from 'src/app/shared/models/docente.model';
import { ActivatedRoute, Router } from '@angular/router';
import { TipoDocumento } from 'src/app/shared/models/tipo-documento.model';

@Component({
  selector: 'app-professor-detail',
  templateUrl: './professor-detail.component.html',
  styleUrls: ['./professor-detail.component.styl']
})
export class ProfessorDetailComponent implements OnInit {
  docente : Docente;
  tiposDocumentos : TipoDocumento[];
  
  constructor(private route : ActivatedRoute, private router : Router ,private service : DocenteService) { }

  ngOnInit() {
    this.cargarTiposDocumentos();
    this.cargarDocente();
  }

  cargarTiposDocumentos() {
    this.service.getTiposDocumentos().subscribe(res => {
      this.tiposDocumentos = res;
    });
  }

  cargarDocente() : void {
    this.route.params.subscribe(params => {
      let id = params['id'];
      if (id) {
        this.service.getDocente(id).subscribe((docente) => {
          this.docente = docente;
          console.log('Carga de docente terminada.');
        });
      }
    });
  }

  editarDocente() {
    this.service.editarDocente(this.docente).subscribe(res => {
      this.router.navigate(['jefe-departamento/docentes/list']);
    });
  }
}
