import { Component, OnInit } from '@angular/core';
import { TipoDocumento } from 'src/app/shared/models/tipo-documento.model';
import { DocenteService } from '../docente.service';
import { Observable } from 'rxjs';
import { Docente } from 'src/app/shared/models/docente.model';
import { Navigation } from 'selenium-webdriver';
import { Persona } from 'src/app/shared/models/persona.model';
import { Contacto } from 'src/app/shared/models/contacto.model';
import { Router } from '@angular/router';
import { GrupoInvestigacion } from 'src/app/shared/models/grupo-investigacion.model';
import { LineaInvestigacion } from 'src/app/shared/models/linea-investigacion.model';

@Component({
  selector: 'app-professor-create',
  templateUrl: './professor-create.component.html',
  styleUrls: ['./professor-create.component.styl']
})

export class ProfessorCreateComponent implements OnInit {

  docente : Docente;
  tipoDocumento : TipoDocumento;
  contacto : Contacto;
  grupoInvestigacion : GrupoInvestigacion;
  lineaInvestigacion : LineaInvestigacion;

  tiposDocumentos : TipoDocumento[];
  gruposInvestigacion : GrupoInvestigacion[];
  lineasInvestigacion : LineaInvestigacion[];

  constructor(private router : Router, private service: DocenteService) {
    this.getTiposDocumentos();
    this.getGruposInvestigacion();
  }

  ngOnInit() {
    this.docente = new Docente();
    this.tipoDocumento = new TipoDocumento();
    this.contacto = new Contacto();
    this.grupoInvestigacion = new GrupoInvestigacion();
    this.lineaInvestigacion = new LineaInvestigacion();
  }

  getTiposDocumentos(): void {
    this.service.getTiposDocumentos().subscribe(documentos => {
      this.tiposDocumentos = documentos;
    });
  }

  getGruposInvestigacion() {
    this.service.getGrupoInvestigacion().subscribe( grupos => {
      this.gruposInvestigacion = grupos;
    });
  }

  onSubmit() {
    this.createDocente();
  }

  createDocente()
  {
    this.docente.objTipoDocumento = this.tipoDocumento;
    this.docente.contacto = this.contacto;
    this.docente.idLineaInvestigacion = this.lineaInvestigacion.id;
    this.service.createDocente(this.docente).subscribe(res => {
      this.router.navigate(['/jefe-departamento/docentes/list']);
    });
  }

  changeGrupoInvestigacion (newObject) {
    this.service.getLineaInvestigacion(newObject).subscribe(
      data => {
        this.lineasInvestigacion = data;
      }
    );
  }

}
