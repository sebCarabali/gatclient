export class Menu{
    constructor(
        public tituloMenu? : string,
        public link? : string,
        public icono? : string,
        public tipo? : string
    ){}
}