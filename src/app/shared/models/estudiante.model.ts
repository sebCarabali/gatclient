import { Persona } from './persona.model';

export class Estudiante {
    constructor(
        public id? : number,
        public nombres? : string,
        public apellidos? : string,
        public persona? : Persona
    ){}
}