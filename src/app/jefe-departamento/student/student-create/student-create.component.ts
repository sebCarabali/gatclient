import { Component, OnInit } from '@angular/core';
import { Estudiante } from 'src/app/shared/models/estudiante.model';
import { Persona } from 'src/app/shared/models/persona.model';
import { TipoDocumento } from 'src/app/shared/models/tipo-documento.model';
import { Contacto } from 'src/app/shared/models/contacto.model';

import { StudentService } from '../student.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-student-create',
  templateUrl: './student-create.component.html',
  styleUrls: ['./student-create.component.styl']
})
export class StudentCreateComponent implements OnInit {

  estudiante : Estudiante;
  persona : Persona;
  tipoDocumento : TipoDocumento;
  contacto : Contacto;
  tiposDocumentos : TipoDocumento[];

  constructor(private router : Router,private service : StudentService) { }

  ngOnInit() {
    this.estudiante = new Estudiante();
    this.persona = new Persona();
    this.tipoDocumento = new TipoDocumento();
    this.contacto = new Contacto();
    this.getTiposDocumentos();
  }

  getTiposDocumentos(): void {
    this.service.getTiposDocumentos().subscribe(documentos => {
      this.tiposDocumentos = documentos;
    });
  }

  crearEstudiante() {
    this.persona.tipoDocumento = this.tipoDocumento;
    this.persona.contacto = this.contacto;
    this.estudiante.persona = this.persona;
    this.service.createEstudiante(this.estudiante).subscribe(res => {
      this.router.navigate(['/jefe-departamento/estudiantes/list']);
    });
  }
}
