import { Component, OnInit } from '@angular/core';
import { MenuService } from '../shared/menu.service';
import { Menu } from '../shared/models/menu.model';
import { AuthenticationService } from '../login/authentication.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.styl']
})
export class MenuComponent implements OnInit {

  menuItems : Menu[];

  constructor(private menuService : MenuService, private authService : AuthenticationService) { }

  ngOnInit() {
    this.cargarMenu();
  }

  cargarMenu() {
    if(this.authService.isUserLoggedIn()) {
      this.menuService.findMenu(sessionStorage.getItem('username')).subscribe(
        data => {
          this.menuItems = data;
          console.log(data);
        }
      )
    }
  }
}
