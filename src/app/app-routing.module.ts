import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login/login.component';
import { LogoutComponent } from './login/logout/logout.component';
import { AuthGaurdService } from './login/auth-gaurd.service';

const routes: Routes = [
  {
    path : 'home',
    component : HomeComponent,
    canActivate : [AuthGaurdService]
  } , {
    path : 'login',
    component : LoginComponent
  }, {
    path : 'logout',
    component : LogoutComponent,
    canActivate : [AuthGaurdService]
  }, {
    path : '',
    component : HomeComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
