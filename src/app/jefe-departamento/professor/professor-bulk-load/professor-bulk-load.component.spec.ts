import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfessorBulkLoadComponent } from './professor-bulk-load.component';

describe('ProfessorBulkLoadComponent', () => {
  let component: ProfessorBulkLoadComponent;
  let fixture: ComponentFixture<ProfessorBulkLoadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfessorBulkLoadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfessorBulkLoadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
