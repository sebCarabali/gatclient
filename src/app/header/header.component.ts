import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../login/authentication.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.styl']
})
export class HeaderComponent implements OnInit {

  usuario : string;

  constructor(private authService : AuthenticationService) { }

  ngOnInit() {
    this.usuario = ""
    this.cargarUsuario();
  }

  cargarUsuario(){
    if(this.authService.isUserLoggedIn()) {
      this.usuario = sessionStorage.getItem('username');
    }
  }

}
