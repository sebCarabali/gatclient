import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Estudiante } from '../../shared/models/estudiante.model';
import { TipoDocumento } from 'src/app/shared/models/tipo-documento.model';


const baseUrl = "http://localhost:8080/GAT/api";
const studentUrl = `${baseUrl}/estudiantes`;
const tipoDocumentoUrl = `${baseUrl}/tipodocumento`;

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  constructor(private http: HttpClient) { }



  getEstudiantes() {
    return this.http.get<Estudiante[]>(studentUrl);
  }

  getEstudiante(id : number) {
    return this.http.get<Estudiante>(`${studentUrl}/${id}`);
  }

  deleteEstudiante(id : number) {
    return this.http.delete(`${studentUrl}/${id}`);
  }

  createEstudiante(estudante: Estudiante) {
    return this.http.post(studentUrl, estudante);
  }

  getTiposDocumentos() {
    return this.http.get<TipoDocumento[]>(tipoDocumentoUrl);
  }

  editarEstudiante(estudiante : Estudiante) {
    return this.http.put(`${studentUrl}/${estudiante.id}`, estudiante);
  }
}
