import { Component, OnInit } from '@angular/core';
import { Estudiante }  from '../../../shared/models/estudiante.model';
import { StudentService } from '../student.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-student-list',
  templateUrl: './student-list.component.html',
  styleUrls: ['./student-list.component.styl']
})
export class StudentListComponent implements OnInit {

  estudiantes : Estudiante[];

  constructor(private router : Router,private service: StudentService) { }

  ngOnInit() {
    this.getEstudiantes();
  }

  getEstudiantes() : void {
    this.service.getEstudiantes().subscribe(estudiantes => {
      this.estudiantes = estudiantes;
    });
  }

  deleteDocente(id : number) {
    this.service.deleteEstudiante(id).subscribe(res => {
      this.router.navigate(['/jefe-departamento/student/list']);
    });
  }
}
