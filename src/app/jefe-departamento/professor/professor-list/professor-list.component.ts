import { Component, OnInit } from '@angular/core';
import { Docente } from 'src/app/shared/models/docente.model';
import {DocenteService} from '../docente.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-professor-list',
  templateUrl: './professor-list.component.html',
  styleUrls: ['./professor-list.component.styl']
})
export class ProfessorListComponent implements OnInit {

  docentes : Docente[];

  constructor(private router : Router, private service : DocenteService) { }

  ngOnInit() {
    this.getDocentes();
  }

  getDocentes() : void {
    this.service.getDocentes().subscribe(docentes => {
      this.docentes = docentes;
    });
  }

  deleteDocente(id : number) {
    this.service.deleteDocente(id).subscribe(res => {
      this.getDocentes();
    });
  }

}
