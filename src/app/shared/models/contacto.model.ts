export class Contacto{
    constructor(
        public id? : number,
        public email? : string,
        public celular? : string,
        public fijo? : string
    ){}
}