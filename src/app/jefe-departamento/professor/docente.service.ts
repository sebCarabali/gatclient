import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

// Models

import { TipoDocumento } from 'src/app/shared/models/tipo-documento.model';
import { Docente } from 'src/app/shared/models/docente.model';
import { GrupoInvestigacion } from 'src/app/shared/models/grupo-investigacion.model';
import { Config } from 'src/app/shared/config.url';
import { LineaInvestigacion } from 'src/app/shared/models/linea-investigacion.model';
//import { Docente } from 'src/app/shared/models/docente.model';


// Constantes
const baseUrl = "http://localhost:8080/GAT/api";
//const baseUrl = "http://localhost:3000";
const tipoDocumentoUrl = `${baseUrl}/tipos/tDocumento`;
const docenteUrl = `${baseUrl}/docentes`;

@Injectable()
export class DocenteService
{

  constructor (private http: HttpClient) {
    
  }

  getDocentes() {
    return this.http.get<Docente[]>(docenteUrl);
  }

  getDocente(id : number)
  {
    return this.http.get<Docente>(`${docenteUrl}/${id}`);
  }

  getTiposDocumentos() {
    return this.http.get<TipoDocumento[]>(tipoDocumentoUrl+"/listar");
  }

  getGrupoInvestigacion() {
    return this.http.get<GrupoInvestigacion[]>(`${Config.baseUrl}tipos/gInvestigacion/listar`);
  }

  getLineaInvestigacion(idGrupo : number){
    return this.http.get<LineaInvestigacion[]>(`${Config.baseUrl}tipos/tLineaInvestigacion/listar/${idGrupo}`);
  }

  createDocente(docente: Docente) {
    return this.http.post(docenteUrl, docente);
  }

  editarDocente(docente : Docente) {
    return this.http.put(`${docenteUrl}/${docente.id}`, docente);
  }

  deleteDocente(id : number) {
    return this.http.delete(`${docenteUrl}/${id}`);
  }
}