import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StudentComponent } from './student/student/student.component';
import { StudentListComponent } from './student/student-list/student-list.component';
import { StudentDetailComponent } from './student/student-detail/student-detail.component';
import { StudentCreateComponent } from './student/student-create/student-create.component';
import { ProfessorComponent } from './professor/professor/professor.component';
import { ProfessorListComponent } from './professor/professor-list/professor-list.component';
import { ProfessorDetailComponent } from './professor/professor-detail/professor-detail.component';
import { ProfessorBulkLoadComponent } from './professor/professor-bulk-load/professor-bulk-load.component';
import { ProfessorCreateComponent } from './professor/professor-create/professor-create.component';
import { AuthGaurdService } from '../login/auth-gaurd.service';

const routes: Routes = [
    {
        path: 'jefe-departamento/docentes',
        component: ProfessorComponent,
        canActivate : [AuthGaurdService],
        children: [
            {
                path: 'list',
                component: ProfessorListComponent,
                canActivate : [AuthGaurdService]
            },
            {
                path: 'detail/:id',
                canActivate : [AuthGaurdService],
                component: ProfessorDetailComponent
            },
            {
                path: 'carga-masiva',
                canActivate : [AuthGaurdService],
                component: ProfessorBulkLoadComponent
            },
            {
                path : 'create',
                canActivate : [AuthGaurdService],
                component : ProfessorCreateComponent
            }
        ]
    }, {
        path : 'jefe-departamento/estudiantes',
        component : StudentComponent,
        canActivate : [AuthGaurdService],
        children : [
            {
                path : "list",
                canActivate : [AuthGaurdService],
                component : StudentListComponent
            },
            {
                path : "create",
                canActivate : [AuthGaurdService],
                component : StudentCreateComponent
            },
            {
                path : "detail/:id",
                canActivate : [AuthGaurdService],
                component : StudentDetailComponent
            }
        ]
    }
];

@NgModule({
    imports : [RouterModule.forChild(routes)],
    exports : [RouterModule]
})
export class JefeDepartamentoRoutingModule {}