import { Component, OnInit } from '@angular/core';
import { Estudiante } from 'src/app/shared/models/estudiante.model';
import { TipoDocumento } from 'src/app/shared/models/tipo-documento.model';
import { ActivatedRoute, Router } from '@angular/router';
import { StudentService } from '../student.service';


@Component({
  selector: 'app-student-detail',
  templateUrl: './student-detail.component.html',
  styleUrls: ['./student-detail.component.styl']
})
export class StudentDetailComponent implements OnInit {

  estudiante: Estudiante;
  tiposDocumentos: TipoDocumento[];

  constructor(private route: ActivatedRoute, private router: Router, private service: StudentService) { }

  ngOnInit() {
    this.cargarDocumentos();
    this.cargarEstudiante();
  }

  cargarDocumentos()
  {
    this.service.getTiposDocumentos().subscribe(docs => {
      this.tiposDocumentos = docs;
    });
  }

  cargarEstudiante() {
    this.route.params.subscribe(params => {
      let id = params['id'];
      if(id) {
        this.service.getEstudiante(id).subscribe(estudiante => {
          this.estudiante = estudiante;
        });
      }
    });
  }

  editarEstudiante() {
    this.service.editarEstudiante(this.estudiante).subscribe(res => {
      this.router.navigate(['jefe-departamento/estudiantes/list']);
    });
  }
}
