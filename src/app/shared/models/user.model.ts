import { Rol } from './Rol';
import { Persona } from './persona.model';

export class User {
    constructor(
        public status? : string,
        public rol? : Rol,
        public persona? : Persona
    ){}
}