import { Persona } from './persona.model';
import { TipoDocumento } from './tipo-documento.model';
import { Contacto } from './contacto.model';

export class Docente {
    constructor(
        public id? : number,
        public nombreCompleto? : string,
        public objTipoDocumento? : TipoDocumento,
        public identificacion? : string,
        public contacto? : Contacto,
        public idLineaInvestigacion? : number
    ){}
}