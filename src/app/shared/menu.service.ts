import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Menu } from './models/menu.model';
import { Config } from  './config.url';


@Injectable({
  providedIn: 'root'
})
export class MenuService {
  constructor(private httpClient : HttpClient) { }

  findMenu(username : string){
    return this.httpClient.get<Menu[]>(`${Config.baseUrl}accesos/menu/listar/${username}`);
  }
}
