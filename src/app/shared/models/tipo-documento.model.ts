export class TipoDocumento
{
    constructor(
        public id? : number,
        public documento? : string
    ){}
}