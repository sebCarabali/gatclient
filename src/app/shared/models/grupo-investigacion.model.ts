export class GrupoInvestigacion{
    constructor(
        public id?: number,
        public categoria?: string,
        public nombreGrupo? : string,
        public acronimo? : string
    ){}
}