import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.styl']
})
export class LoginComponent implements OnInit {

  username : string;
  password : string;
  invalidLogin : boolean;

  constructor(private service : AuthenticationService,
    private router : Router) { 
    
  }

  ngOnInit() {
    this.invalidLogin = false;
  }

  checkLogin() {
    if(this.service.authenticate(this.username, this.password).subscribe(data => {
      console.log(data);
    })){
      this.invalidLogin = false;
      this.router.navigate(['/home']);
    } else {
      this.invalidLogin = true;
    }
  }

}
